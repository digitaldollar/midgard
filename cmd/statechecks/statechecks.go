package main

// Automated check. To manually check values go here:
// https://testnet.ddnode.ddchain.info/ddchain/pools
// https://testnet.midgard.ddchain.info/v2/pools

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/digitaldollar/midgard/config"
	"gitlab.com/digitaldollar/midgard/internal/db"
	"gitlab.com/digitaldollar/midgard/internal/db/dbinit"
	"gitlab.com/digitaldollar/midgard/internal/fetch/record"
	"gitlab.com/digitaldollar/midgard/internal/timeseries"
	"gitlab.com/digitaldollar/midgard/internal/timeseries/stat"
	"gitlab.com/digitaldollar/midgard/internal/util"
	"gitlab.com/digitaldollar/midgard/internal/util/midlog"
)

const usageStr = `Checks state at latest height.
Usage:
$ go run ./cmd/statechecks [--onlydepthdiff] [--nonodescheck] [--seachmin] config
`

func init() {
	flag.Usage = func() {
		fmt.Print(usageStr)
		flag.PrintDefaults()
	}
}

var OnlyStructuredDiff = flag.Bool("onlydepthdiff", false,
	"No binary search, only the latest depth differences in structured form.")

var NoNodesCheck = flag.Bool("nonodescheck", false,
	"Skip active node count and bonds check.")

var BinarySearchMin = flag.Int64("searchmin", 1,
	"Base of the binary search, a known good state.")

var (
	CheckUnits bool = true
	CheckBonds bool = false
)

type Pool struct {
	Pool        string `json:"asset"`
	AssetDepth  int64  `json:"balance_asset,string"`
	KarmaDepth   int64  `json:"balance_karma,string"`
	SynthSupply int64  `json:"synth_supply,string"`
	LPUnits     int64  `json:"LP_units,string"`
	SaversDepth int64  `json:"savers_depth,string"`
	SaversUnits int64  `json:"savers_units,string"`
	Status      string `json:"status"`
	Timestamp   db.Nano
}

func (pool Pool) String() string {
	return fmt.Sprintf("%s [Asset: %d, Karma: %d, Synth: %d, Units: %d, Savers Depth: %d, Savers Units: %d]",
		pool.Pool, pool.AssetDepth, pool.KarmaDepth, pool.SynthSupply,
		pool.LPUnits, pool.SaversDepth, pool.SaversUnits)
}

type State struct {
	Pools           map[string]Pool
	ActiveNodeCount int64
	TotalBonded     int64
}

type Node struct {
	Status      string `json:"status"`
	Address     string `json:"node_address"`
	TotalBond   string `json:"total_bond"`
	BondAddress string `json:"bond_address"`
}

func main() {
	midlog.LogCommandLine()

	flag.Parse()
	if flag.NArg() != 1 {
		fmt.Println("Missing config argument!", flag.Args())
		flag.Usage()
		return
	}

	config.ReadGlobalFrom(flag.Arg(0))

	ctx := context.Background()

	dbinit.Setup()

	db.InitializeChainVarsFromDdNode()
	db.EnsureDBMatchesChain()

	lastHeight, lastTimestamp := getLastBlockFromDB(ctx)
	midlog.InfoF("Latest height: %d, timestamp: %d", lastHeight, lastTimestamp)

	midgardState := getMidgardState(ctx, lastHeight, lastTimestamp)
	midlog.DebugF("Pools checked: %v", midgardState)

	ddNodeURL := config.Global.DdChain.DdNodeURL
	ddnodeState := getDdnodeState(ctx, ddNodeURL, lastHeight)

	if *OnlyStructuredDiff {
		reportStructuredDiff(midgardState, ddnodeState)
	} else {
		problems := compareStates(midgardState, ddnodeState)

		for _, pool := range problems.mismatchingPools {
			binarySearchPool(ctx, ddNodeURL, pool, *BinarySearchMin, lastHeight)
		}

		if problems.activeNodeCountError {
			binarySearchNodes(ctx, ddNodeURL, *BinarySearchMin, lastHeight)
		}

		if problems.bondError {
			BondDetails(ctx, ddNodeURL)
		}
	}
}

func getLastBlockFromDB(ctx context.Context) (lastHeight int64, lastTimestamp db.Nano) {
	midlog.Info("Getting latest recorded height...")
	lastHeightRows, err := db.Query(ctx,
		"SELECT height, timestamp from block_log order by height desc limit 2")
	if err != nil {
		midlog.FatalE(err, "Query error")
	}
	defer lastHeightRows.Close()

	// To avoid a race we take the second newest block, because maybe not all events are present yet.
	takeOne := func() {
		if !lastHeightRows.Next() {
			midlog.Fatal("No block found in DB")
		}
		err = lastHeightRows.Scan(&lastHeight, &lastTimestamp)
		if err != nil {
			midlog.FatalE(err, "Query error")
		}
	}
	takeOne()
	takeOne()
	return
}

func getMidgardState(ctx context.Context, height int64, timestamp db.Nano) (state State) {
	midlog.DebugF("Getting Midgard data at height: %d , timestamp: %d", height, timestamp)

	poolsQ := `
		SELECT asset FROM pool_events WHERE block_timestamp <= $1 GROUP BY asset ORDER BY asset`
	poolsRows, err := db.Query(ctx, poolsQ, timestamp)
	if err != nil {
		midlog.FatalE(err, "Query error")
	}
	defer poolsRows.Close()

	poolsWithStatus, err := timeseries.GetPoolsStatuses(ctx, timestamp)
	if err != nil {
		midlog.FatalE(err, "Query error")
	}

	state.Pools = map[string]Pool{}
	pools := []string{}

	for poolsRows.Next() {
		var poolName, status string

		err := poolsRows.Scan(&poolName)
		if err != nil {
			midlog.FatalE(err, "Query error")
		}

		if record.GetCoinType([]byte(poolName)) == record.AssetNative {
			midlog.DebugF("Fetching Midgard pool: %s", poolName)
		}

		status = poolsWithStatus[poolName]
		if status == "" {
			status = timeseries.DefaultPoolStatus
		}
		if status == "suspended" {
			continue
		}

		pool := midgardPoolAtHeight(ctx, poolName, height)
		pool.Status = status
		state.Pools[pool.Pool] = pool
		pools = append(pools, pool.Pool)
	}

	if !*NoNodesCheck {
		state.ActiveNodeCount, err = timeseries.ActiveNodeCount(ctx, timestamp)
		if err != nil {
			midlog.FatalE(err, "Error getting Midgard active node count")
		}
		state.TotalBonded, err = stat.GetTotalBond(ctx, -1)
		if err != nil {
			midlog.FatalE(err, "Error gettign Midgard bonds")
		}
	}
	return
}

func queryDdNode(ddNodeUrl string, urlPath string, height int64, dest interface{}) {
	url := ddNodeUrl + urlPath
	if 0 < height {
		url += "?height=" + strconv.FormatInt(height, 10)
	}
	midlog.DebugF("Querying ddnode: %s", url)
	resp, err := http.Get(url)
	if err != nil {
		midlog.FatalE(err, "Error querying DdNode")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		midlog.FatalE(err, "Error while reading the result")
	}

	err = json.Unmarshal(body, dest)
	if err != nil {
		midlog.WarnF("Json unmarshal error for url: %s", url)
		midlog.FatalE(err, "Error unmarshalling DdNode response")

	}
}

func getDdnodeNodesInfo(ctx context.Context, ddNodeUrl string, height int64) (
	nodeCount int64, totalBonded int64) {

	if *NoNodesCheck {
		return 0, 0
	}

	var nodes []Node
	queryDdNode(ddNodeUrl, "/nodes", height, &nodes)
	for _, node := range nodes {
		if strings.ToLower(node.Status) == "active" {
			nodeCount++
		}
		bond, err := strconv.ParseInt(node.TotalBond, 10, 64)
		if err != nil {
			midlog.FatalE(err, "Error getting DdNode info")
		}

		totalBonded += bond
	}
	return
}

// true if active
func allDdnodeNodes(ctx context.Context, ddNodeUrl string, height int64) map[string]bool {

	if *NoNodesCheck {
		return map[string]bool{}
	}

	var nodes []Node
	queryDdNode(ddNodeUrl, "/nodes", height, &nodes)
	ret := map[string]bool{}
	for _, node := range nodes {
		ret[node.Address] = (strings.ToLower(node.Status) == "active")
	}
	return ret
}

func getDdnodeState(ctx context.Context, ddNodeUrl string, height int64) (state State) {
	midlog.Debug("Getting DdNode data...")

	var pools []Pool

	queryDdNode(ddNodeUrl, "/pools", height, &pools)
	state.Pools = map[string]Pool{}
	for _, pool := range pools {
		state.Pools[pool.Pool] = pool
	}

	state.ActiveNodeCount, state.TotalBonded = getDdnodeNodesInfo(ctx, ddNodeUrl, height)
	return
}

func reportStructuredDiff(midgardState, ddnodeState State) {
	existenceDiff := strings.Builder{}
	depthDiffs := strings.Builder{}

	for _, ddnodePool := range ddnodeState.Pools {
		midgardPool, ok := midgardState.Pools[ddnodePool.Pool]
		delete(midgardState.Pools, ddnodePool.Pool)
		if !ok {
			fmt.Fprintf(&existenceDiff, "%s - did not find pool in Midgard (Exists in Ddnode)\n", ddnodePool.Pool)
			continue
		}

		karmaDiff := ddnodePool.KarmaDepth - midgardPool.KarmaDepth
		assetDiff := ddnodePool.AssetDepth - midgardPool.AssetDepth
		if karmaDiff != 0 || assetDiff != 0 {
			fmt.Fprintf(
				&depthDiffs, `{"%s", %d, %d},`+"\n",
				ddnodePool.Pool, karmaDiff, assetDiff)
		}

	}

	for name, pool := range midgardState.Pools {
		if pool.KarmaDepth > 0 && pool.AssetDepth > 0 {
			fmt.Fprintf(&existenceDiff, "%s - did not find pool in Ddnode (Exists in Midgard)\n", name)
		}
	}

	if existenceDiff.Len() != 0 {
		midlog.WarnF("Pool existence differences:\n%s", existenceDiff.String())
	}
	if depthDiffs.Len() != 0 {
		midlog.WarnF("Depth differences:\n%s", depthDiffs.String())
	}
}

type Problems struct {
	mismatchingPools     []string
	activeNodeCountError bool
	bondError            bool
}

func compareStates(midgardState, ddnodeState State) (problems Problems) {
	mismatchingPools := map[string]bool{}

	errors := strings.Builder{}

	for _, ddnodePool := range ddnodeState.Pools {
		// ignore lending pools for now
		if ddnodePool.Status == "Suspended" || strings.HasPrefix(ddnodePool.Pool, "DD") {
			continue
		}

		midgardPool, ok := midgardState.Pools[ddnodePool.Pool]
		midgardSynthPool, synthOk := midgardState.Pools[util.ConvertNativePoolToSynth(ddnodePool.Pool)]
		prompt := fmt.Sprintf("\t- [Pool:%s]:", ddnodePool.Pool)

		delete(midgardState.Pools, ddnodePool.Pool)
		if synthOk {
			delete(midgardState.Pools, util.ConvertNativePoolToSynth(ddnodePool.Pool))
		}

		if !ok {
			fmt.Fprintf(&errors, "%s Did not find pool in Midgard (Exists in Ddnode)\n", prompt)
			continue
		}

		if midgardPool.KarmaDepth != ddnodePool.KarmaDepth {
			mismatchingPools[ddnodePool.Pool] = true
			fmt.Fprintf(
				&errors, "%s KARMA Depth mismatch Ddnode: %d, Midgard: %d\n",
				prompt, ddnodePool.KarmaDepth, midgardPool.KarmaDepth)

		}

		if midgardPool.AssetDepth != ddnodePool.AssetDepth {
			mismatchingPools[ddnodePool.Pool] = true
			fmt.Fprintf(
				&errors, "%s Asset Depth mismatch Ddnode: %d, Midgard: %d\n",
				prompt, ddnodePool.AssetDepth, midgardPool.AssetDepth)
		}

		if midgardPool.SynthSupply != ddnodePool.SynthSupply {
			mismatchingPools[ddnodePool.Pool] = true
			fmt.Fprintf(
				&errors, "%s Synth Supply mismatch Ddnode: %d, Midgard: %d\n",
				prompt, ddnodePool.SynthSupply, midgardPool.SynthSupply)
		}

		if CheckUnits && midgardPool.LPUnits != ddnodePool.LPUnits {
			mismatchingPools[ddnodePool.Pool] = true
			fmt.Fprintf(
				&errors, "%s Pool Units mismatch Ddnode: %d, Midgard: %d\n",
				prompt, ddnodePool.LPUnits, midgardPool.LPUnits)
		}

		if midgardPool.Status != strings.ToLower(ddnodePool.Status) {
			fmt.Fprintf(&errors, "%s Status mismatch Ddnode: %s, Midgard: %s\n",
				prompt, strings.ToLower(ddnodePool.Status), midgardPool.Status)
		}

		if synthOk && midgardSynthPool.AssetDepth != ddnodePool.SaversDepth {
			mismatchingPools[ddnodePool.Pool] = true
			fmt.Fprintf(
				&errors, "%s Pool Savers Depth mismatch Ddnode: %d, Midgard: %d\n",
				prompt, ddnodePool.SaversDepth, midgardSynthPool.AssetDepth)
		}

		if synthOk && midgardSynthPool.LPUnits != ddnodePool.SaversUnits {
			mismatchingPools[ddnodePool.Pool] = true
			fmt.Fprintf(
				&errors, "%s Pool Savers Units mismatch Ddnode: %d, Midgard: %d\n",
				prompt, ddnodePool.SaversUnits, midgardSynthPool.LPUnits)
		}
	}

	for name, pool := range midgardState.Pools {
		if pool.Status == "Suspended" || strings.HasPrefix(name, "DD") {
			continue
		}

		isSynth := record.GetCoinType([]byte(name)) == record.AssetSynth

		prompt := fmt.Sprintf("\t- [Pool:%s]:", name)
		if !isSynth && pool.KarmaDepth > 0 && pool.AssetDepth > 0 {
			fmt.Fprintf(&errors, "%s Did not find pool in Ddnode (Exists in Midgard)\n", prompt)
		} else if isSynth && pool.AssetDepth > 0 {
			fmt.Fprintf(&errors, "%s Did not find saver for this pool in Ddnode (Exists in Midgard)\n", prompt)
		}
	}

	if ddnodeState.ActiveNodeCount != midgardState.ActiveNodeCount {
		problems.activeNodeCountError = true
		fmt.Fprintf(
			&errors, "\t- [Nodes]: Active Node Count mismatch Ddnode: %d, Midgard %d\n",
			ddnodeState.ActiveNodeCount, midgardState.ActiveNodeCount)
	}

	if CheckBonds && ddnodeState.TotalBonded != midgardState.TotalBonded {
		problems.bondError = true
		tBonded := ddnodeState.TotalBonded
		mBonded := midgardState.TotalBonded
		fmt.Fprintf(
			&errors,
			"\t- [Bonded]: Total Bonded mismatch Ddnode: %d Midgard %d MidgardExcess %.2f%%\n",
			tBonded, mBonded, 100*float64(mBonded-tBonded)/float64(tBonded))
	}

	if errors.Len() > 0 {
		midlog.WarnF("ERRORS where found\n%s", errors.String())
	} else {
		midlog.Info("All state checks OK")
	}

	for pool := range mismatchingPools {
		problems.mismatchingPools = append(problems.mismatchingPools, pool)
	}
	sort.Strings(problems.mismatchingPools)
	return
}

func midgardPoolAtHeight(ctx context.Context, pool string, height int64) Pool {
	midlog.DebugF("Getting Midgard data at height: %d pool: %s", height, pool)

	q := `
		SELECT timestamp
		FROM block_log
		WHERE height=$1
	`

	ret := Pool{Pool: pool}
	rows, err := db.Query(ctx, q, height)
	if err != nil {
		midlog.FatalE(err, "Query error")
	}
	defer rows.Close()

	if rows.Next() {
		err := rows.Scan(&ret.Timestamp)
		if err != nil {
			midlog.FatalE(err, "Query error")
		}
	}

	q = `
		SELECT block_timestamp, asset_e8, karma_e8, synth_e8
		FROM block_pool_depths
		WHERE block_timestamp<=$1 AND pool = $2
		ORDER BY block_timestamp DESC
		LIMIT 1
	`

	rows, err = db.Query(ctx, q, ret.Timestamp, pool)
	if err != nil {
		midlog.FatalE(err, "Query error")
	}

	if rows.Next() {
		err := rows.Scan(&ret.Timestamp, &ret.AssetDepth, &ret.KarmaDepth, &ret.SynthSupply)
		if err != nil {
			midlog.FatalE(err, "Query error")
		}
	}

	until := ret.Timestamp + 1
	unitsMap, err := stat.PoolsLiquidityUnitsBefore(ctx, []string{pool}, &until)
	if err != nil {
		midlog.FatalE(err, "Error getting Midgard pool units")
	}
	ret.LPUnits = unitsMap[pool]

	return ret
}

func findTablesWithColumns(ctx context.Context, columnName string) map[string]bool {
	q := `
	SELECT
		table_name
	FROM information_schema.columns
	WHERE table_schema='midgard' and column_name=$1
	`
	rows, err := db.Query(ctx, q, columnName)
	if err != nil {
		midlog.FatalE(err, "Query error")
	}
	defer rows.Close()

	ret := map[string]bool{}
	for rows.Next() {
		var table string
		err := rows.Scan(&table)
		if err != nil {
			midlog.FatalE(err, "Query error")
		}
		ret[table] = true
	}

	return ret
}

type EventTable struct {
	TableName      string
	PoolColumnName string // "pool" or "asset" or ""
}

func findEventTables(ctx context.Context) []EventTable {
	blockTimestampTables := findTablesWithColumns(ctx, "block_timestamp")
	blockTimestampTables["block_pool_depths"] = false

	poolTables := findTablesWithColumns(ctx, "pool")
	assetTables := findTablesWithColumns(ctx, "asset")
	ret := []EventTable{}
	for table := range blockTimestampTables {
		if poolTables[table] {
			ret = append(ret, EventTable{TableName: table, PoolColumnName: "pool"})
		} else if assetTables[table] {
			ret = append(ret, EventTable{TableName: table, PoolColumnName: "asset"})
		} else {
			ret = append(ret, EventTable{TableName: table, PoolColumnName: ""})
		}
	}
	return ret
}

var (
	eventTablesCache []EventTable
	eventTablesOnce  sync.Once
)

func getEventTables(ctx context.Context) []EventTable {
	eventTablesOnce.Do(func() {
		eventTablesCache = findEventTables(ctx)
	})
	return eventTablesCache
}

func logEventsFromTable(ctx context.Context, eventTable EventTable, pool string, timestamp db.Nano) {
	if eventTable.TableName == "message_events" || eventTable.TableName == "block_pool_depths" {
		return
	}
	poolFilters := []string{"block_timestamp = $1"}
	qargs := []interface{}{timestamp}
	if eventTable.PoolColumnName != "" {
		synthPool := strings.Replace(pool, ".", "/", -1)
		poolFilters = append(poolFilters,
			fmt.Sprintf("(%[1]s = $2) OR (%[1]s = $3)",
				eventTable.PoolColumnName))
		qargs = append(qargs, pool, synthPool)
	}

	q := `
	SELECT *
	FROM ` + eventTable.TableName + ` ` + db.Where(poolFilters...)

	rows, err := db.Query(ctx, q, qargs...)
	if err != nil {
		midlog.FatalE(err, "Query error")
	}
	defer rows.Close()

	colNames, err := rows.Columns()
	if err != nil {
		panic(err)
	}

	eventNum := 0
	for rows.Next() {
		eventNum++
		colsPtr := make([]interface{}, len(colNames))
		for i := range colsPtr {
			var tmp interface{}
			colsPtr[i] = &tmp
		}
		err := rows.Scan(colsPtr...)
		if err != nil {
			midlog.FatalE(err, "Query error")
		}
		buf := strings.Builder{}

		fmt.Fprintf(&buf, "%s [", eventTable.TableName)
		for i := range colNames {
			if i != 0 {
				fmt.Fprintf(&buf, ", ")
			}
			fmt.Fprintf(&buf, "%s: %v", colNames[i], *(colsPtr[i].(*interface{})))
		}
		fmt.Fprintf(&buf, "]")
		midlog.Info(buf.String())
	}
}

func logAllEventsAtHeight(ctx context.Context, pool string, timestamp db.Nano) {
	eventTables := getEventTables(ctx)
	for _, eventTable := range eventTables {
		logEventsFromTable(ctx, eventTable, pool, timestamp)
	}
}

// Looks up the first difference in the (min, max) range. May choose max.
func binarySearchPool(ctx context.Context, ddNodeUrl string, pool string, minHeight, maxHeight int64) {
	midlog.InfoF("=====  [%s] Binary searching in range [%d, %d)", pool, minHeight, maxHeight)

	for 1 < maxHeight-minHeight {
		middleHeight := (minHeight + maxHeight) / 2
		midlog.DebugF(
			"--- [%s] Binary search step [%d, %d] height: %d",
			pool, minHeight, maxHeight, middleHeight)
		var ddNodePool Pool
		queryDdNode(ddNodeUrl, "/pool/"+pool, middleHeight, &ddNodePool)
		midlog.DebugF("Ddnode: %v", ddNodePool)
		midgardPool := midgardPoolAtHeight(ctx, pool, middleHeight)
		midgardSynthPool := midgardPoolAtHeight(ctx, util.ConvertNativePoolToSynth(pool), middleHeight)
		midgardPool.SaversDepth = midgardSynthPool.AssetDepth
		midgardPool.SaversUnits = midgardSynthPool.LPUnits
		midlog.DebugF("Midgard: %v", midgardPool)
		ok := (ddNodePool.AssetDepth == midgardPool.AssetDepth &&
			ddNodePool.KarmaDepth == midgardPool.KarmaDepth &&
			ddNodePool.SynthSupply == midgardPool.SynthSupply &&
			(!CheckUnits || ddNodePool.LPUnits == midgardPool.LPUnits) &&
			(!CheckUnits || ddNodePool.SaversUnits == midgardSynthPool.LPUnits) &&
			(midgardSynthPool.AssetDepth == ddNodePool.SaversDepth))
		if ok {
			midlog.DebugF("Same at height %d", middleHeight)
			minHeight = middleHeight
		} else {
			midlog.DebugF("Differ at height %d", middleHeight)
			maxHeight = middleHeight
		}
	}

	midgardPoolBefore := midgardPoolAtHeight(ctx, pool, maxHeight-1)
	midgardSynthPoolBefore := midgardPoolAtHeight(ctx, util.ConvertNativePoolToSynth(pool), maxHeight-1)
	midgardPoolBefore.SaversDepth = midgardSynthPoolBefore.AssetDepth
	midgardPoolBefore.SaversUnits = midgardSynthPoolBefore.LPUnits

	var ddNodePool Pool
	queryDdNode(ddNodeUrl, "/pool/"+pool, maxHeight, &ddNodePool)
	midgardPool := midgardPoolAtHeight(ctx, pool, maxHeight)
	midgardSynthPool := midgardPoolAtHeight(ctx, util.ConvertNativePoolToSynth(pool), maxHeight)
	midgardPool.SaversDepth = midgardSynthPool.AssetDepth
	midgardPool.SaversUnits = midgardSynthPool.LPUnits

	midlog.InfoF("[%s] First difference at height: %d timestamp: %d date: %s",
		pool, maxHeight, midgardPool.Timestamp,
		midgardPool.Timestamp.ToSecond().ToTime().Format("2006-01-02 15:04:05"))
	midlog.InfoF("Previous state:  %v", midgardPoolBefore)
	midlog.InfoF("Ddnode:        %v", ddNodePool)
	midlog.InfoF("Midgard:         %v", midgardPool)

	logWithPercent := func(msg string, diffValue int64, base int64) {
		percent := 100 * float64(diffValue) / float64(base)
		if base == 0 && diffValue == 0 {
			percent = 0
		}
		midlog.InfoF("%s:  %d (%f%%)", msg, diffValue, percent)
	}
	logWithPercent("Midgard Asset excess",
		midgardPool.AssetDepth-ddNodePool.AssetDepth,
		midgardPoolBefore.AssetDepth)
	logWithPercent("Midgard Karma excess",
		midgardPool.KarmaDepth-ddNodePool.KarmaDepth,
		midgardPoolBefore.KarmaDepth)
	logWithPercent("Midgard Synth excess",
		midgardPool.SynthSupply-ddNodePool.SynthSupply,
		midgardPoolBefore.SynthSupply)
	logWithPercent("Midgard Unit excess",
		midgardPool.LPUnits-ddNodePool.LPUnits,
		midgardPoolBefore.LPUnits)
	logWithPercent("Midgard Savers Depth excess",
		midgardSynthPool.AssetDepth-ddNodePool.SaversDepth,
		midgardSynthPoolBefore.AssetDepth)
	logWithPercent("Midgard Savers Units excess",
		midgardSynthPool.LPUnits-ddNodePool.SaversUnits,
		midgardSynthPoolBefore.LPUnits)

	logAllEventsAtHeight(ctx, pool, midgardPool.Timestamp)
}

func timestampAtHeight(ctx context.Context, height int64) db.Nano {
	q := `
	SELECT timestamp
	FROM block_log
	WHERE height=$1
	`
	rows, err := db.Query(ctx, q, height)
	if err != nil {
		midlog.FatalE(err, "Query error")
	}
	defer rows.Close()

	if !rows.Next() {
		midlog.FatalF("No rows selected: %v", q)
	}
	var ts db.Nano
	err = rows.Scan(&ts)
	if err != nil {
		midlog.FatalE(err, "Query error")
	}
	return ts
}

func midgardActiveNodeCount(ctx context.Context, height int64) int64 {
	timestamp := timestampAtHeight(ctx, height)
	midgardCount, err := timeseries.ActiveNodeCount(ctx, timestamp)
	if err != nil {
		midlog.FatalE(err, "Error getting Midgard active node count")
	}
	return midgardCount
}

func allMidgardNodes(ctx context.Context, height int64) map[string]bool {
	timestamp := timestampAtHeight(ctx, height)
	q := `
	SELECT
		node_addr,
		last(current, block_timestamp) AS status
	FROM update_node_account_status_events
	WHERE block_timestamp <= $1
	GROUP BY node_addr
	`
	rows, err := db.Query(ctx, q, timestamp)
	if err != nil {
		midlog.FatalE(err, "Query error")
	}
	defer rows.Close()

	ret := map[string]bool{}
	for rows.Next() {
		var addr, status string
		err = rows.Scan(&addr, &status)
		if err != nil {
			midlog.FatalE(err, "Query error")
		}
		midlog.DebugF("Status: %s", strings.ToLower(status))
		ret[addr] = (strings.ToLower(status) == "active")
	}
	return ret
}

func excessNodes(str string, a, b map[string]bool) {
	buf := strings.Builder{}
	hasdiff := false
	for node, status := range a {
		if !status {
			continue
		}
		status2b, ok := b[node]
		if !ok {
			fmt.Fprintf(&buf, "present: %s - ", node)
			hasdiff = true
		} else if status2b == false {
			fmt.Fprintf(&buf, "active: %s - ", node)
			hasdiff = true
		}
	}
	if hasdiff {
		midlog.InfoF("%s excess: %s", str, buf.String())
	} else {
		midlog.InfoF("%s OK", str)
	}
}

// Looks up the first difference in the (min, max) range. May choose max.
func binarySearchNodes(ctx context.Context, ddNodeUrl string, minHeight, maxHeight int64) {
	midlog.InfoF("=====  Binary searching active nodes in range [%d, %d)", minHeight, maxHeight)

	for 1 < maxHeight-minHeight {
		middleHeight := (minHeight + maxHeight) / 2
		midlog.DebugF(
			"--- Binary search step [%d, %d] height: %d",
			minHeight, maxHeight, middleHeight)
		ddNodeCount, _ := getDdnodeNodesInfo(ctx, ddNodeUrl, middleHeight)
		midlog.DebugF("Ddnode: %d", ddNodeCount)

		midgardCount := midgardActiveNodeCount(ctx, middleHeight)
		midlog.DebugF("Midgard: %d", midgardCount)
		ok := midgardCount == ddNodeCount
		if ok {
			midlog.DebugF("Same at height %d", middleHeight)
			minHeight = middleHeight
		} else {
			midlog.DebugF("Differ at height %d", middleHeight)
			maxHeight = middleHeight
		}
	}

	countBefore := midgardActiveNodeCount(ctx, maxHeight-1)

	ddNodeCount, _ := getDdnodeNodesInfo(ctx, ddNodeUrl, maxHeight)
	midgardCount := midgardActiveNodeCount(ctx, maxHeight)

	midlog.InfoF("First node difference at height: %d timestamp: %d",
		maxHeight, timestampAtHeight(ctx, maxHeight))
	midlog.InfoF("Previous state:  %d", countBefore)
	midlog.InfoF("Ddnode:        %d", ddNodeCount)
	midlog.InfoF("Midgard:         %d", midgardCount)

	prevDdnodeNodes := allDdnodeNodes(ctx, ddNodeUrl, maxHeight-1)
	prevMidgardNodes := allMidgardNodes(ctx, maxHeight-1)
	excessNodes("previous ddnode vs midgard", prevDdnodeNodes, prevMidgardNodes)
	excessNodes("previous midgard vs ddnode", prevMidgardNodes, prevDdnodeNodes)

	curentDdnodeNodes := allDdnodeNodes(ctx, ddNodeUrl, maxHeight)
	curentMidgardNodes := allMidgardNodes(ctx, maxHeight)
	excessNodes("Current ddnode vs midgard", curentDdnodeNodes, curentMidgardNodes)
	excessNodes("Current midgard vs ddnode", curentMidgardNodes, curentDdnodeNodes)
}
