// This tool checks if pool units reported reported for each member sum by DdNode sums up to the
// total units of the pool.
// At the time of writing it does.
package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/digitaldollar/midgard/config"
	"gitlab.com/digitaldollar/midgard/internal/api"
	"gitlab.com/digitaldollar/midgard/internal/db"
	"gitlab.com/digitaldollar/midgard/internal/db/dbinit"
	"gitlab.com/digitaldollar/midgard/internal/fetch/record"
	"gitlab.com/digitaldollar/midgard/internal/timeseries"
	"gitlab.com/digitaldollar/midgard/internal/util/midlog"
)

const usageStr = `Check pool units share of each member
Usage:
$ go run ./cmd/membercheck config pool heightOrBlockTimestamp
or
$ go run ./cmd/membercheck --allpools  config heightOrBlockTimestamp
`

func init() {
	flag.Usage = func() {
		fmt.Print(usageStr)
		flag.PrintDefaults()
	}
}

var AllPoolsStructured = flag.Bool("allpools", false,
	"No binary search, only the latest depth differences in structured form.")

type DdNodeSummary struct {
	TotalUnits int64 `json:"LP_units,string"`
}

// MemberChange may represent the state of a Member, an Add or a Withdraw.
// In case of withdraw the units are negative.
type MemberChange struct {
	Units        int64  `json:"units,string"`
	KarmaAddress  string `json:"karma_address"`
	AssetAddress string `json:"asset_address"`
}

func main() {
	midlog.LogCommandLine()

	flag.Parse()
	if flag.NArg() < 1 {
		fmt.Println("Not enough arguments!")
		flag.Usage()
		return
	}

	config.ReadGlobalFrom(flag.Arg(0))

	dbinit.Setup()

	db.InitializeChainVarsFromDdNode()
	db.EnsureDBMatchesChain()

	if *AllPoolsStructured {
		CheckAllPoolsStructured()
	} else {
		CheckOnePool()
	}
}

func findHeight(param string) (height int64, timestamp db.Nano) {
	idStr := param

	heightOrTimestamp, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		midlog.FatalF("Couldn't parse height or timestamp: %s", idStr)
	}
	height, timestamp, err = api.TimestampAndHeight(context.Background(), heightOrTimestamp)
	if err != nil {
		midlog.FatalE(err, "Couldn't find height or timestamp. ")
	}
	return
}

func CheckOnePool() {
	if flag.NArg() != 3 {
		fmt.Println("provide 3 args!")
		flag.Usage()
		return
	}

	ctx := context.Background()
	pool := flag.Arg(1)
	idStr := flag.Arg(2)

	height, timestamp := findHeight(idStr)
	ddNodeMembers := getDdNodeMembers(pool, height)
	midlog.DebugF("Ddnode karma addresses: %d assetOnly addresses: %d assetToKarmaMap: %d",
		len(ddNodeMembers.KarmaMemberUnits),
		len(ddNodeMembers.AssetMemberUnits),
		len(ddNodeMembers.AssetToKarmaMap))

	midgardMembers := getMidgardMembers(ctx, pool, timestamp)
	midlog.DebugF("Midgard karma addresses: %d assetOnly addresses: %d assetToKarmaMap: %d",
		len(midgardMembers.KarmaMemberUnits),
		len(midgardMembers.AssetMemberUnits),
		len(midgardMembers.AssetToKarmaMap))

	memberDiff(ddNodeMembers, midgardMembers)
}

func CheckAllPoolsStructured() {
	if flag.NArg() != 2 {
		fmt.Println("provide 2 args!")
		flag.Usage()
		return
	}

	ctx := context.Background()
	idStr := flag.Arg(1)

	height, timestamp := findHeight(idStr)

	poolsWithStatus, err := timeseries.GetPoolsStatuses(ctx, timestamp)
	if err != nil {
		midlog.FatalE(err, "Error getting Midgard pool status")
	}
	sortedPools := []string{}
	for k := range poolsWithStatus {
		sortedPools = append(sortedPools, k)
	}
	sort.Strings(sortedPools)
	for _, pool := range sortedPools {
		status := poolsWithStatus[pool]
		if status == "suspended" {
			continue
		}
		ddNodeMembers := getDdNodeMembers(pool, height)
		midlog.DebugF(
			"Ddnode karma addresses: %d assetOnly addresses: %d assetToKarmaMap: %d",
			len(ddNodeMembers.KarmaMemberUnits),
			len(ddNodeMembers.AssetMemberUnits),
			len(ddNodeMembers.AssetToKarmaMap))

		midgardMembers := getMidgardMembers(ctx, pool, timestamp)
		midlog.DebugF(
			"Midgard karma addresses: %d assetOnly addresses: %d assetToKarmaMap: %d",
			len(midgardMembers.KarmaMemberUnits), len(midgardMembers.AssetMemberUnits), len(midgardMembers.AssetToKarmaMap))

		saveStructuredDiffs(pool, ddNodeMembers, midgardMembers)
	}
	printStructuredDiffs()
}

type MemberMap struct {
	// KarmaMemberUnits is keyed by karma address if the member has one, otherwise asset address
	KarmaMemberUnits map[string]int64

	// Only if it doesn't have a karma address
	AssetMemberUnits map[string]int64

	// Disjoint with AssetMemberUnits
	AssetToKarmaMap map[string]string
}

func NewMemberMap() MemberMap {
	return MemberMap{
		KarmaMemberUnits:  map[string]int64{},
		AssetMemberUnits: map[string]int64{},
		AssetToKarmaMap:   map[string]string{},
	}
}

func check(ok bool, v ...interface{}) {
	if !ok {
		log.Fatal(v...)
	}
}

func (x *MemberMap) AddMemberSimple(m MemberChange) {
	rAddr := m.KarmaAddress
	aAddr := m.AssetAddress

	if rAddr != "" {
		x.KarmaMemberUnits[rAddr] += m.Units
	} else {
		check(aAddr != "", "Empty karma and asset address")
		x.AssetMemberUnits[aAddr] += m.Units
	}
}

// If there is an asset address without karma address
// then it looks for previous usage of the asset address to find an adequate karma adresses.
func (x *MemberMap) AddMemberClustered(m MemberChange) {
	rAddr := m.KarmaAddress
	aAddr := m.AssetAddress

	if rAddr != "" {
		x.KarmaMemberUnits[rAddr] += m.Units

		if aAddr != "" {
			assetUnits, previouslyAssetOnly := x.AssetMemberUnits[aAddr]
			if previouslyAssetOnly {
				x.KarmaMemberUnits[rAddr] += assetUnits
				delete(x.AssetMemberUnits, aAddr)
			}

			previousKarmaAddr, assetAddrAlreadyRegistered := x.AssetToKarmaMap[aAddr]
			if assetAddrAlreadyRegistered {
				if previousKarmaAddr != rAddr {
					midlog.FatalF("AssetAddress registered with multiple karma addresses %s %s",
						rAddr, previousKarmaAddr)
				}
			} else {
				x.AssetToKarmaMap[aAddr] = m.KarmaAddress
			}
		}
	} else {
		check(aAddr != "", "Empty karma and asset address")
		previousKarmaAddr, hasKarmaPair := x.AssetToKarmaMap[aAddr]
		if hasKarmaPair {
			x.KarmaMemberUnits[previousKarmaAddr] += m.Units
		} else {
			x.AssetMemberUnits[aAddr] += m.Units
		}
	}
}

func (x *MemberMap) RemoveZero() {
	for k, v := range x.KarmaMemberUnits {
		if v == 0 {
			delete(x.KarmaMemberUnits, k)
		}
	}
	for k, v := range x.AssetMemberUnits {
		if v == 0 {
			delete(x.AssetMemberUnits, k)
		}
	}
}

func mapSum(m map[string]int64) int64 {
	var ret int64
	for _, v := range m {
		ret += v
	}
	return ret
}

func (x *MemberMap) TotalUnits() int64 {
	return mapSum(x.KarmaMemberUnits) + mapSum(x.AssetMemberUnits)
}

func getDdNodeMembers(pool string, height int64) MemberMap {
	midlog.InfoF("Checking pool units sum. Pool: %s Height: %d", pool, height)

	ddNodeURL := config.Global.DdChain.DdNodeURL

	var summary DdNodeSummary
	queryDdNode(ddNodeURL, "/pool/"+pool, height, &summary)
	midlog.InfoF("DdNode global units: %d", summary.TotalUnits)

	var ddnodeBreakdown []MemberChange
	queryDdNode(ddNodeURL, "/pool/"+pool+"/liquidity_providers", height, &ddnodeBreakdown)

	ret := NewMemberMap()

	var sum2 int64
	for _, member := range ddnodeBreakdown {
		sum2 += member.Units
		ret.AddMemberSimple(member)
	}
	midlog.InfoF("DdNode units per member summed up: %d", sum2)
	if sum2 == summary.TotalUnits {
		midlog.Info("ddnode is consistent")
	} else {
		midlog.FatalF(
			"ddnode INCONSISTENT.\nPools Total units: %d\n Member units sum: %d\nDiff: %d",
			summary.TotalUnits,
			sum2,
			summary.TotalUnits-sum2,
		)
	}

	ret.RemoveZero()
	return ret
}

func queryDdNode(ddNodeUrl string, urlPath string, height int64, dest interface{}) {
	url := ddNodeUrl + urlPath
	if 0 < height {
		url += "?height=" + strconv.FormatInt(height, 10)
	}
	midlog.DebugF("Querying ddnode: %s", url)
	resp, err := http.Get(url)
	if err != nil {
		midlog.FatalE(err, "Querying DdNode")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	err = json.Unmarshal(body, dest)
	if err != nil {
		midlog.FatalE(err, "Error unmarshaling DdNode response")
	}
}

func getMidgardMembers(ctx context.Context, pool string, timestamp db.Nano) MemberMap {
	ret := NewMemberMap()

	addQ := `
		SELECT karma_addr, asset_addr, stake_units
		FROM stake_events
		WHERE pool = $1 and block_timestamp <= $2
		ORDER BY block_timestamp
	`
	addRows, err := db.Query(ctx, addQ, pool, timestamp)
	if err != nil {
		midlog.FatalE(err, "Query error")
	}
	defer addRows.Close()

	for addRows.Next() {
		var karmaAddress, assetAddress sql.NullString
		var add MemberChange
		err := addRows.Scan(
			&karmaAddress,
			&assetAddress,
			&add.Units)
		if err != nil {
			midlog.FatalE(err, "Query error")
		}
		if karmaAddress.Valid {
			add.KarmaAddress = karmaAddress.String
		}
		if assetAddress.Valid {
			add.AssetAddress = assetAddress.String
		}
		ret.AddMemberSimple(add)
	}

	withdrawQ := `
		SELECT from_addr, stake_units
		FROM withdraw_events
		WHERE pool = $1 and block_timestamp <= $2
		ORDER BY block_timestamp
	`

	withdrawRows, err := db.Query(ctx, withdrawQ, pool, timestamp)
	if err != nil {
		midlog.FatalE(err, "Query error")
	}
	defer withdrawRows.Close()

	for withdrawRows.Next() {
		var fromAddr string
		var units int64
		err := withdrawRows.Scan(
			&fromAddr,
			&units)
		if err != nil {
			midlog.FatalE(err, "Query error")
		}
		withdraw := MemberChange{Units: -units}
		if record.AddressIsKarma(fromAddr) {
			withdraw.KarmaAddress = fromAddr
		} else {
			withdraw.AssetAddress = fromAddr
		}
		ret.AddMemberClustered(withdraw)
	}

	ret.RemoveZero()
	return ret
}

func mapDiff(ddNodeMap map[string]int64, midgardMap map[string]int64) {
	diffCount := 0
	for k, tValue := range ddNodeMap {
		mValue, mOk := midgardMap[k]
		if !mOk {
			midlog.WarnF("Missing address in Midgard: %s DdNode units: %d", k, tValue)
			diffCount++
		} else if mValue != tValue {
			midlog.WarnF(
				"Mismatch units for address: %s  DdNode: %d  Midgard: %d  diff: %d",
				k, tValue, mValue, tValue-mValue)
			diffCount++
		}
	}
	for k, mValue := range midgardMap {
		_, tOk := ddNodeMap[k]
		if !tOk {
			midlog.WarnF("Extra address in Midgard: %s  Midgard units: %d", k, mValue)
			diffCount++
		}
	}
	if diffCount == 0 {
		midlog.Info("No difference")
	}
}

func memberDiff(ddNodeMembers MemberMap, midgardMembers MemberMap) {
	midlog.Info("Checking Karma adresses")
	mapDiff(ddNodeMembers.KarmaMemberUnits, midgardMembers.KarmaMemberUnits)
	midlog.Info("Checking Asset adresses")
	mapDiff(ddNodeMembers.AssetMemberUnits, midgardMembers.AssetMemberUnits)

	ddNodeUnits := ddNodeMembers.TotalUnits()
	midgardUnits := midgardMembers.TotalUnits()
	if ddNodeUnits != midgardUnits {
		midlog.WarnF("Total units mismatch. DdNode: %d  Midgard: %d", ddNodeUnits, midgardUnits)
	} else {
		midlog.Info("Total units are equal")
	}
}

var structuredBuff strings.Builder

func saveStructuredDiffs(pool string, ddNodeMembers MemberMap, midgardMembers MemberMap) {
	diffValue := map[string]int64{}
	accumulate := func(ddNodeMap map[string]int64, midgardMap map[string]int64) {
		for k, tValue := range ddNodeMap {
			mValue, mOk := midgardMap[k]
			if !mOk {
				diffValue[k] = tValue
			} else if mValue != tValue {
				diffValue[k] = tValue - mValue
			}
		}
		for k, mValue := range midgardMap {
			_, tOk := ddNodeMap[k]
			if !tOk {
				diffValue[k] = -mValue
			}
		}
	}
	accumulate(ddNodeMembers.KarmaMemberUnits, midgardMembers.KarmaMemberUnits)
	accumulate(ddNodeMembers.AssetMemberUnits, midgardMembers.AssetMemberUnits)

	keys := []string{}
	for k := range diffValue {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		v := diffValue[k]
		fmt.Fprintf(&structuredBuff, `{"%s", "%s", %d},`+"\n", pool, k, v)
	}
}

func printStructuredDiffs() {
	midlog.InfoF("Needed changes to Midgard:\n%v", structuredBuff.String())
}
