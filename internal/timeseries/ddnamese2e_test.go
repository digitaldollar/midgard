package timeseries_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/digitaldollar/midgard/internal/db/testdb"
	"gitlab.com/digitaldollar/midgard/internal/timeseries"
	"gitlab.com/digitaldollar/midgard/openapi/generated/oapigen"
)

func TestDDNamesE2E(t *testing.T) {
	blocks := testdb.InitTestBlocks(t)

	setupLastBlock := int64(100)
	timeseries.SetLastHeightForTest(setupLastBlock)

	dd1 := "dd1xxxx"
	dd2 := "dd2xxxx"
	dd3 := "dd3xxxx"
	btc1 := "bc1xxxx"
	btc2 := "bc2xxxx"
	eth1 := "0x1xxxx"

	// setup a happy ddname
	blocks.NewBlock(t, "2000-01-01 00:00:00",
		testdb.DDName{
			Name:            "test1",
			Chain:           "DD",
			Address:         dd1,
			Owner:           dd1,
			RegistrationFee: 10_00000000,
			FundAmount:      1_00000000,
			ExpireHeight:    123456,
		},
		testdb.DDName{
			Name:            "test1",
			Chain:           "BTC",
			Address:         btc1,
			Owner:           dd1,
			RegistrationFee: 0,
			FundAmount:      0,
			ExpireHeight:    123456,
		},
	)
	var lookup oapigen.DDNameDetailsResponse
	body := testdb.CallJSON(t, "http://localhost:8080/v2/ddname/lookup/test1")
	testdb.MustUnmarshal(t, body, &lookup)

	require.Equal(t, 2, len(lookup.Entries))
	require.Equal(t, dd1, lookup.Owner)
	require.Equal(t, "123456", lookup.Expire)
	require.Equal(t, "BTC", lookup.Entries[0].Chain)
	require.Equal(t, btc1, lookup.Entries[0].Address)
	require.Equal(t, "DD", lookup.Entries[1].Chain)
	require.Equal(t, dd1, lookup.Entries[1].Address)

	// Test chainging ownership of happy ddname
	blocks.NewBlock(t, "2000-01-01 00:02:00",
		testdb.DDName{
			Name:            "test1",
			Chain:           "DD",
			Address:         dd2,
			Owner:           dd2,
			RegistrationFee: 0,
			FundAmount:      1_00000000,
			ExpireHeight:    1234567,
		},
		testdb.DDName{
			Name:            "test1",
			Chain:           "BTC",
			Address:         btc2,
			Owner:           dd2,
			RegistrationFee: 0,
			FundAmount:      0,
			ExpireHeight:    1234567,
		},
	)
	body = testdb.CallJSON(t, "http://localhost:8080/v2/ddname/lookup/test1")
	testdb.MustUnmarshal(t, body, &lookup)

	require.Equal(t, 2, len(lookup.Entries))
	require.Equal(t, dd2, lookup.Owner)
	require.Equal(t, "1234567", lookup.Expire)
	require.Equal(t, "BTC", lookup.Entries[0].Chain)
	require.Equal(t, btc2, lookup.Entries[0].Address)
	require.Equal(t, "DD", lookup.Entries[1].Chain)
	require.Equal(t, dd2, lookup.Entries[1].Address)

	// check that an expired ddname doesn't show up
	blocks.NewBlock(t, "2000-01-01 00:03:00",
		testdb.DDName{
			Name:            "expired",
			Chain:           "DD",
			Address:         dd1,
			Owner:           dd1,
			RegistrationFee: 10_00000000,
			FundAmount:      1_00000000,
			ExpireHeight:    1,
		},
	)
	testdb.CallFail(t, "http://localhost:8080/v2/ddname/lookup/expired", "not found")

	// Test reverse lookup, but first create a ddname, where the user is no longer the owner
	blocks.NewBlock(t, "2000-01-01 00:04:00",
		testdb.DDName{
			Name:            "test2",
			Chain:           "DD",
			Address:         dd2,
			Owner:           dd2,
			RegistrationFee: 0,
			FundAmount:      1_00000000,
			ExpireHeight:    4000,
		},
	)
	blocks.NewBlock(t, "2000-01-01 00:05:00",
		testdb.DDName{
			Name:            "test2",
			Chain:           "DD",
			Address:         dd3,
			Owner:           dd3,
			RegistrationFee: 0,
			FundAmount:      1_00000000,
			ExpireHeight:    4000,
		},
	)
	blocks.NewBlock(t, "2000-01-01 00:06:00",
		testdb.DDName{
			Name:            "test3",
			Chain:           "DD",
			Address:         dd2,
			Owner:           dd2,
			RegistrationFee: 0,
			FundAmount:      1_00000000,
			ExpireHeight:    4000,
		},
	)
	var rlookup oapigen.ReverseDDNameResponse
	testdb.CallFail(t, "http://localhost:8080/v2/ddname/rlookup/"+dd1, "not found")
	testdb.CallFail(t, "http://localhost:8080/v2/ddname/rlookup/"+btc1, "not found")

	body = testdb.CallJSON(t, "http://localhost:8080/v2/ddname/rlookup/"+dd2)
	testdb.MustUnmarshal(t, body, &rlookup)
	require.Equal(t, 2, len(rlookup))
	require.Equal(t, "test1", rlookup[0])
	require.Equal(t, "test3", rlookup[1])

	//Test renewing the ddname and add new chain and also, changing btc address
	blocks.NewBlock(t, "2000-01-01 00:07:00",
		testdb.DDName{
			Name:            "test1",
			Chain:           "DD",
			Address:         dd2,
			Owner:           dd2,
			RegistrationFee: 0,
			FundAmount:      1_00000000,
			ExpireHeight:    3,
		},
		testdb.DDName{
			Name:            "test1",
			Chain:           "BTC",
			Address:         btc1,
			Owner:           dd2,
			RegistrationFee: 0,
			FundAmount:      0,
			ExpireHeight:    3,
		},
	)

	blocks.NewBlock(t, "2000-01-01 00:08:00",
		testdb.DDName{
			Name:            "test1",
			Chain:           "ETH",
			Address:         eth1,
			Owner:           dd2,
			RegistrationFee: 0,
			FundAmount:      0,
			ExpireHeight:    3,
		},
	)

	testdb.CallFail(t, "http://localhost:8080/v2/ddname/lookup/test1", "not found")
	testdb.CallFail(t, "http://localhost:8080/v2/ddname/rlookup/"+eth1, "not found")

	blocks.NewBlock(t, "2000-01-01 00:09:00",
		testdb.DDName{
			Name:            "test1",
			Chain:           "DD",
			Address:         dd2,
			Owner:           dd2,
			RegistrationFee: 0,
			FundAmount:      1_00000000,
			ExpireHeight:    1000000,
		},
	)

	body = testdb.CallJSON(t, "http://localhost:8080/v2/ddname/lookup/test1")
	testdb.MustUnmarshal(t, body, &lookup)

	require.Equal(t, 3, len(lookup.Entries))
	require.Equal(t, dd2, lookup.Owner)
	require.Equal(t, "1000000", lookup.Expire)
	require.Equal(t, "BTC", lookup.Entries[0].Chain)
	require.Equal(t, btc1, lookup.Entries[0].Address)
	require.Equal(t, "ETH", lookup.Entries[1].Chain)
	require.Equal(t, eth1, lookup.Entries[1].Address)
	require.Equal(t, "DD", lookup.Entries[2].Chain)
	require.Equal(t, dd2, lookup.Entries[2].Address)
}

func TestDDNamesCaseInsensitive(t *testing.T) {
	blocks := testdb.InitTestBlocks(t)

	// setup a happy ddname
	blocks.NewBlock(t, "2000-01-01 00:00:00",
		testdb.DDName{
			Name:            "name1",
			Chain:           "ETH",
			Address:         "EthADDR1",
			Owner:           "DdAddr1",
			RegistrationFee: 10_00000000,
			FundAmount:      1_00000000,
			ExpireHeight:    123456,
		},
		testdb.DDName{
			Name:            "name2",
			Chain:           "BTC",
			Address:         "BTCaddr2",
			Owner:           "DdAddr2",
			RegistrationFee: 10_00000000,
			FundAmount:      1_00000000,
			ExpireHeight:    123456,
		},
	)

	var rlookup oapigen.ReverseDDNameResponse

	{
		body := testdb.CallJSON(t, "http://localhost:8080/v2/ddname/rlookup/ethaddr1")
		testdb.MustUnmarshal(t, body, &rlookup)
		require.Equal(t, 1, len(rlookup))
		require.Equal(t, "name1", rlookup[0])
	}

	{
		body := testdb.CallJSON(t, "http://localhost:8080/v2/ddname/rlookup/EthaDDr1")
		testdb.MustUnmarshal(t, body, &rlookup)
		require.Equal(t, 1, len(rlookup))
		require.Equal(t, "name1", rlookup[0])
	}

	{
		body := testdb.CallJSON(t, "http://localhost:8080/v2/ddname/rlookup/BTCaddr2")
		testdb.MustUnmarshal(t, body, &rlookup)
		require.Equal(t, 1, len(rlookup))
		require.Equal(t, "name2", rlookup[0])
	}

	testdb.JSONFailGeneral(t, "http://localhost:8080/v2/ddname/rlookup/bTcAdDr2")

	{
		body := testdb.CallJSON(t, "http://localhost:8080/v2/ddname/lookup/name1")
		var lookup oapigen.DDNameDetailsResponse
		testdb.MustUnmarshal(t, body, &lookup)

		require.Equal(t, 1, len(lookup.Entries))
		require.Equal(t, "DdAddr1", lookup.Owner)
	}
}

func TestDDNamesOwner(t *testing.T) {
	blocks := testdb.InitTestBlocks(t)

	blocks.NewBlock(t, "2000-01-01 00:00:00",
		testdb.DDName{
			Name:         "name1",
			Chain:        "DD",
			Address:      "ddTarget",
			Owner:        "ddOwner",
			FundAmount:   1_00000000,
			ExpireHeight: 3,
		},
	)

	var rlookup oapigen.ReverseDDNameResponse

	// rlookup by owner fails
	testdb.CallFail(t, "http://localhost:8080/v2/ddname/rlookup/ddOwner", "not found")

	body := testdb.CallJSON(t, "http://localhost:8080/v2/ddname/owner/ddOwner")
	testdb.MustUnmarshal(t, body, &rlookup)

	require.Equal(t, "name1", rlookup[0])

	// Add a few blocks, let it expire.
	blocks.NewBlock(t, "2000-01-01 00:00:01")
	blocks.NewBlock(t, "2000-01-01 00:00:02")
	blocks.NewBlock(t, "2000-01-01 00:00:03")
	blocks.NewBlock(t, "2000-01-01 00:00:04")

	testdb.CallFail(t, "http://localhost:8080/v2/ddname/owner/ddOwner", "not found")

	// Reenable DdName
	blocks.NewBlock(t, "2000-01-01 00:00:05",
		testdb.DDName{
			Name:         "name1",
			Chain:        "DD",
			Address:      "ddTarget",
			Owner:        "ddOwner",
			FundAmount:   1_00000000,
			ExpireHeight: 100,
		},
	)
	body = testdb.CallJSON(t, "http://localhost:8080/v2/ddname/owner/ddOwner")
	testdb.MustUnmarshal(t, body, &rlookup)
	require.Equal(t, "name1", rlookup[0])

	// Register a differ owner & overwrite the older owner
	blocks.NewBlock(t, "2000-01-01 00:00:06",
		testdb.DDName{
			Name:         "name1",
			Chain:        "DD",
			Address:      "ddTarget",
			Owner:        "ddDifferentOwner",
			FundAmount:   1_00000000,
			ExpireHeight: 99,
		},
	)

	testdb.CallFail(t, "http://localhost:8080/v2/ddname/owner/ddOwner", "not found")

	body = testdb.CallJSON(t, "http://localhost:8080/v2/ddname/owner/ddDifferentOwner")
	testdb.MustUnmarshal(t, body, &rlookup)
	require.Equal(t, "name1", rlookup[0])

	// ddOnwer has two ddnames on its behalf and renewing its DD (Chain) name with a new address
	blocks.NewBlock(t, "2000-01-01 00:00:07",
		testdb.DDName{
			Name:         "name2",
			Chain:        "BTC",
			Address:      "btcTarget",
			Owner:        "ddOwner",
			FundAmount:   1_00000000,
			ExpireHeight: 99,
		},
		testdb.DDName{
			Name:         "name1",
			Chain:        "DD",
			Address:      "ddOwner",
			Owner:        "ddOwner",
			FundAmount:   1_00000000,
			ExpireHeight: 100,
		},
		testdb.DDName{
			Name:         "name1",
			Chain:        "BTC",
			Address:      "btcTarget",
			Owner:        "ddOwner",
			FundAmount:   1_00000000,
			ExpireHeight: 101,
		},
	)

	body = testdb.CallJSON(t, "http://localhost:8080/v2/ddname/owner/ddOwner")
	testdb.MustUnmarshal(t, body, &rlookup)
	require.Equal(t, 2, len(rlookup))
	require.Equal(t, "name1", rlookup[0])
	require.Equal(t, "name2", rlookup[1])
}
