package stat

import (
	"fmt"
	"math"
	"net/http"

	"gitlab.com/digitaldollar/midgard/config"
	"gitlab.com/digitaldollar/midgard/internal/timeseries"
)

func karmaPriceUSDForDepths(depths timeseries.DepthMap) float64 {
	ret := math.NaN()
	var maxdepth int64 = -1

	for _, pool := range config.Global.UsdPools {
		poolInfo, ok := depths[pool]
		if ok && maxdepth < poolInfo.KarmaDepth {
			maxdepth = poolInfo.KarmaDepth
			ret = 1 / poolInfo.AssetPrice()
		}
	}
	return ret
}

// Returns the 1/price from the depest whitelisted pool.
func KarmaPriceUSD() float64 {
	return karmaPriceUSDForDepths(timeseries.Latest.GetState().Pools)
}

func ServeUSDDebug(resp http.ResponseWriter, req *http.Request) {
	state := timeseries.Latest.GetState()
	for _, pool := range config.Global.UsdPools {
		poolInfo := state.PoolInfo(pool)
		if poolInfo == nil {
			fmt.Fprintf(resp, "%s - pool not found\n", pool)
		} else {
			depth := float64(poolInfo.KarmaDepth) / 1e8
			karmaPrice := 1 / poolInfo.AssetPrice()
			fmt.Fprintf(resp, "%s - karmaDepth: %.0f karmaPriceUsd: %.2f\n", pool, depth, karmaPrice)
		}
	}

	fmt.Fprintf(resp, "\n\nkarmaPriceUSD: %v", KarmaPriceUSD())
}
