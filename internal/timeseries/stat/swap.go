package stat

import (
	"context"
	"errors"

	"gitlab.com/digitaldollar/midgard/internal/db"
)

// Swaps are generic swap statistics.
type Swaps struct {
	TxCount     int64
	KarmaE8Total int64
}

type SwapBucket struct {
	StartTime         db.Second
	EndTime           db.Second
	KarmaToAssetCount  int64
	AssetToKarmaCount  int64
	KarmaToSynthCount  int64
	SynthToKarmaCount  int64
	TotalCount        int64
	KarmaToAssetVolume int64
	AssetToKarmaVolume int64
	KarmaToSynthVolume int64
	SynthToKarmaVolume int64
	TotalVolume       int64
	KarmaToAssetFees   int64
	AssetToKarmaFees   int64
	KarmaToSynthFees   int64
	SynthToKarmaFees   int64
	TotalFees         int64
	KarmaToAssetSlip   int64
	AssetToKarmaSlip   int64
	KarmaToSynthSlip   int64
	SynthToKarmaSlip   int64
	TotalSlip         int64
	KarmaPriceUSD      float64
}

func (sb *SwapBucket) writeOneDirection(oneDirection *OneDirectionSwapBucket) {
	switch oneDirection.Direction {
	case db.KarmaToAsset:
		sb.KarmaToAssetCount += oneDirection.Count
		sb.KarmaToAssetVolume += oneDirection.VolumeInKarma
		sb.KarmaToAssetFees += oneDirection.TotalFees
		sb.KarmaToAssetSlip += oneDirection.TotalSlip
	case db.AssetToKarma:
		sb.AssetToKarmaCount += oneDirection.Count
		sb.AssetToKarmaVolume += oneDirection.VolumeInKarma
		sb.AssetToKarmaFees += oneDirection.TotalFees
		sb.AssetToKarmaSlip += oneDirection.TotalSlip
	case db.KarmaToSynth:
		sb.KarmaToSynthCount += oneDirection.Count
		sb.KarmaToSynthVolume += oneDirection.VolumeInKarma
		sb.KarmaToSynthFees += oneDirection.TotalFees
		sb.KarmaToSynthSlip += oneDirection.TotalSlip
	case db.SynthToKarma:
		sb.SynthToKarmaCount += oneDirection.Count
		sb.SynthToKarmaVolume += oneDirection.VolumeInKarma
		sb.SynthToKarmaFees += oneDirection.TotalFees
		sb.SynthToKarmaSlip += oneDirection.TotalSlip
	}
}

func (sb *SwapBucket) calculateTotals() {
	sb.TotalCount = (sb.KarmaToAssetCount + sb.AssetToKarmaCount +
		sb.KarmaToSynthCount + sb.SynthToKarmaCount)
	sb.TotalVolume = (sb.KarmaToAssetVolume + sb.AssetToKarmaVolume +
		sb.KarmaToSynthVolume + sb.SynthToKarmaVolume)
	sb.TotalFees = (sb.KarmaToAssetFees + sb.AssetToKarmaFees +
		sb.KarmaToSynthFees + sb.SynthToKarmaFees)
	sb.TotalSlip = (sb.KarmaToAssetSlip + sb.AssetToKarmaSlip +
		sb.KarmaToSynthSlip + sb.SynthToKarmaSlip)
}

// Used to sum up the buckets in the meta
func (meta *SwapBucket) AddBucket(bucket SwapBucket) {
	meta.KarmaToAssetCount += bucket.KarmaToAssetCount
	meta.AssetToKarmaCount += bucket.AssetToKarmaCount
	meta.KarmaToSynthCount += bucket.KarmaToSynthCount
	meta.SynthToKarmaCount += bucket.SynthToKarmaCount
	meta.TotalCount += bucket.TotalCount
	meta.KarmaToAssetVolume += bucket.KarmaToAssetVolume
	meta.AssetToKarmaVolume += bucket.AssetToKarmaVolume
	meta.KarmaToSynthVolume += bucket.KarmaToSynthVolume
	meta.SynthToKarmaVolume += bucket.SynthToKarmaVolume
	meta.TotalVolume += bucket.TotalVolume
	meta.KarmaToAssetFees += bucket.KarmaToAssetFees
	meta.AssetToKarmaFees += bucket.AssetToKarmaFees
	meta.KarmaToSynthFees += bucket.KarmaToSynthFees
	meta.SynthToKarmaFees += bucket.SynthToKarmaFees
	meta.TotalFees += bucket.TotalFees
	meta.KarmaToAssetSlip += bucket.KarmaToAssetSlip
	meta.AssetToKarmaSlip += bucket.AssetToKarmaSlip
	meta.KarmaToSynthSlip += bucket.KarmaToSynthSlip
	meta.SynthToKarmaSlip += bucket.SynthToKarmaSlip
	meta.TotalSlip += bucket.TotalSlip
}

type OneDirectionSwapBucket struct {
	Time         db.Second
	Count        int64
	VolumeInKarma int64
	TotalFees    int64
	TotalSlip    int64
	Direction    db.SwapDirection
}

var SwapsAggregate = db.RegisterAggregate(db.NewAggregate("swaps", "swap_events").
	AddGroupColumn("pool").
	AddGroupColumn("_direction").
	AddSumlikeExpression("volume_e8",
		`SUM(CASE
			WHEN _direction%2 = 0 THEN from_e8
			WHEN _direction%2 = 1 THEN to_e8 + liq_fee_in_karma_e8
			ELSE 0 END)::BIGINT`).
	AddSumlikeExpression("swap_count", "COUNT(1)").
	// On swapping from asset to karma fees are collected in karma.
	AddSumlikeExpression("karma_fees_e8",
		"SUM(CASE WHEN _direction%2 = 1 THEN liq_fee_e8 ELSE 0 END)::BIGINT").
	// On swapping from karma to asset fees are collected in asset.
	AddSumlikeExpression("asset_fees_e8",
		"SUM(CASE WHEN _direction%2 = 0 THEN liq_fee_e8 ELSE 0 END)::BIGINT").
	AddBigintSumColumn("liq_fee_in_karma_e8").
	AddBigintSumColumn("swap_slip_bp"))

// Returns sparse buckets, when there are no swaps in the bucket, the bucket is missing.
// Returns several results for a given for all directions where a swap is present.
func GetSwapBuckets(ctx context.Context, pool *string, buckets db.Buckets) (
	[]OneDirectionSwapBucket, error) {

	filters := []string{}
	params := []interface{}{}
	if pool != nil {
		filters = append(filters, "pool = $1")
		params = append(params, *pool)
	}
	q, params := SwapsAggregate.BucketedQuery(`
		SELECT
			aggregate_timestamp/1000000000 as time,
			_direction,
			SUM(swap_count) AS count,
			SUM(volume_e8) AS volume,
			SUM(liq_fee_in_karma_e8) AS fee,
			SUM(swap_slip_bp) AS slip
		FROM %s
		GROUP BY _direction, time
		ORDER BY time ASC
	`, buckets, filters, params)

	rows, err := db.Query(ctx, q, params...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	ret := []OneDirectionSwapBucket{}
	for rows.Next() {
		var bucket OneDirectionSwapBucket
		err := rows.Scan(&bucket.Time, &bucket.Direction, &bucket.Count, &bucket.VolumeInKarma, &bucket.TotalFees, &bucket.TotalSlip)
		if err != nil {
			return []OneDirectionSwapBucket{}, err
		}
		ret = append(ret, bucket)
	}
	return ret, rows.Err()
}

// Does not fill USD field of the SwapBucket
// If pool is nil, returns global
func GetOneIntervalSwapsNoUSD(
	ctx context.Context, pool *string, buckets db.Buckets) (
	*SwapBucket, error) {

	if !buckets.OneInterval() {
		return nil, errors.New("Single interval buckets expected for swapsNoUSD")
	}
	swaps, err := GetSwapBuckets(ctx, pool, buckets)
	if err != nil {
		return nil, err
	}

	ret := SwapBucket{
		StartTime: buckets.Start(),
		EndTime:   buckets.End(),
	}
	for _, swap := range swaps {
		if swap.Time != buckets.Start() {
			return nil, errors.New("Bad returned timestamp while reading swap stats")
		}
		ret.writeOneDirection(&swap)
	}
	ret.calculateTotals()
	return &ret, nil
}

// Returns gapfilled PoolSwaps for given pool, window and interval
func GetPoolSwaps(ctx context.Context, pool *string, buckets db.Buckets) ([]SwapBucket, error) {
	swaps, err := GetSwapBuckets(ctx, pool, buckets)
	if err != nil {
		return nil, err
	}
	usdPrice, err := USDPriceHistory(ctx, buckets)
	if err != nil {
		return nil, err
	}

	return mergeSwapsGapfill(swaps, usdPrice), nil
}

func mergeSwapsGapfill(swaps []OneDirectionSwapBucket,
	denseUSDPrices []USDPriceBucket) []SwapBucket {
	ret := make([]SwapBucket, len(denseUSDPrices))

	timeAfterLast := denseUSDPrices[len(denseUSDPrices)-1].Window.Until + 1
	swaps = append(swaps, OneDirectionSwapBucket{Time: timeAfterLast})

	idx := 0
	for i, usdPrice := range denseUSDPrices {
		current := &ret[i]
		current.StartTime = usdPrice.Window.From
		current.EndTime = usdPrice.Window.Until
		for swaps[idx].Time == current.StartTime {
			swap := &swaps[idx]
			current.writeOneDirection(swap)
			idx++
		}

		current.calculateTotals()
		current.KarmaPriceUSD = usdPrice.KarmaPriceUSD
	}

	return ret
}

func addVolumes(
	ctx context.Context,
	pools []string,
	w db.Window,
	poolVolumes *map[string]int64) error {

	bucket := db.OneIntervalBuckets(w.From, w.Until)
	wheres := []string{
		"pool = ANY($1)",
	}
	params := []interface{}{pools}

	q, params := SwapsAggregate.BucketedQuery(`
	SELECT
		pool,
		volume_e8 AS volume
	FROM %s
	`, bucket, wheres, params)

	swapRows, err := db.Query(ctx, q, params...)
	if err != nil {
		return err
	}
	defer swapRows.Close()

	for swapRows.Next() {
		var pool string
		var volume int64
		err := swapRows.Scan(&pool, &volume)
		if err != nil {
			return err
		}
		(*poolVolumes)[pool] += volume
	}
	return nil
}

// PoolsTotalVolume computes total volume amount for given timestamps (from/to) and pools
func PoolsTotalVolume(ctx context.Context, pools []string, w db.Window) (map[string]int64, error) {
	poolVolumes := make(map[string]int64)
	err := addVolumes(ctx, pools, w, &poolVolumes)
	if err != nil {
		return nil, err
	}

	return poolVolumes, nil
}

type CountVolume = struct {
	Count  int64
	Volume int64
}
type SwapStats map[db.SwapDirection]CountVolume

func (s SwapStats) Totals() (total CountVolume) {
	for _, cv := range s {
		total.Count += cv.Count
		total.Volume += cv.Volume
	}
	return
}

func GlobalSwapStats(ctx context.Context, aggregate string, start db.Second) (SwapStats, error) {
	q := `
		SELECT
			_direction,
			SUM(volume_e8),
			SUM(swap_count)
		FROM midgard_agg.swaps_` + aggregate + `
		WHERE aggregate_timestamp >= $1
		GROUP BY _direction`

	rows, err := db.Query(ctx, q, start.ToNano().ToI())
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	res := make(SwapStats)
	for rows.Next() {
		var dir db.SwapDirection
		var volume, count int64
		err = rows.Scan(&dir, &volume, &count)
		if err != nil {
			return nil, err
		}
		res[dir] = CountVolume{count, volume}
	}

	return res, nil
}
