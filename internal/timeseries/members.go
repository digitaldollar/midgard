package timeseries

import (
	"context"

	"github.com/lib/pq"
	"gitlab.com/digitaldollar/midgard/internal/db"
	"gitlab.com/digitaldollar/midgard/internal/fetch/record"
	"gitlab.com/digitaldollar/midgard/internal/util"
	"gitlab.com/digitaldollar/midgard/openapi/generated/oapigen"
)

// GetMemberIds returns the ids of all known members.
//
// The id of a member is defined as their karma address if they are participating with their karma
// address, or as their asset address otherwise (for members with asset address only.)
//
// Member ids present in multiple pools will be only returned once.
func GetMemberIds(ctx context.Context, pool *string) (addrs []string, err error) {
	poolFilter := ""
	qargs := []interface{}{}
	if pool != nil {
		poolFilter = "pool = $1"
		qargs = append(qargs, pool)
	}

	q := "SELECT DISTINCT member_id FROM midgard_agg.members " + db.Where(poolFilter)

	rows, err := db.Query(ctx, q, qargs...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var member string
		err := rows.Scan(&member)
		if err != nil {
			return nil, err
		}
		addrs = append(addrs, member)
	}

	return addrs, nil
}

func GetBorrowerIds(ctx context.Context, asset *string) (addrs []string, err error) {
	assetFilter := ""
	qargs := []interface{}{}
	if asset != nil {
		assetFilter = "collateral_asset = $1"
		qargs = append(qargs, asset)
	}

	q := "SELECT DISTINCT borrower_id FROM midgard_agg.borrowers " + db.Where(assetFilter)

	rows, err := db.Query(ctx, q, qargs...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var borrower string
		err := rows.Scan(&borrower)
		if err != nil {
			return nil, err
		}
		addrs = append(addrs, borrower)
	}

	return addrs, nil
}

// TODO(HooriRn): this struct might not be needed since the graphql depracation. (delete-graphql)
// Info of a member in a specific pool.
type MemberPool struct {
	Pool           string
	KarmaAddress    string
	AssetAddress   string
	LiquidityUnits int64
	AssetDeposit   int64
	KarmaDeposit    int64
	KarmaAdded      int64
	AssetAdded     int64
	KarmaPending    int64
	AssetPending   int64
	DateFirstAdded int64
	DateLastAdded  int64
	KarmaWithdrawn  int64
	AssetWithdrawn int64
}

func (memberPool MemberPool) toOapigen() oapigen.MemberPool {
	return oapigen.MemberPool{
		Pool:           memberPool.Pool,
		KarmaAddress:    memberPool.KarmaAddress,
		AssetAddress:   memberPool.AssetAddress,
		LiquidityUnits: util.IntStr(memberPool.LiquidityUnits),
		KarmaDeposit:    util.IntStr(memberPool.KarmaDeposit),
		AssetDeposit:   util.IntStr(memberPool.AssetDeposit),
		KarmaAdded:      util.IntStr(memberPool.KarmaAdded),
		AssetAdded:     util.IntStr(memberPool.AssetAdded),
		KarmaWithdrawn:  util.IntStr(memberPool.KarmaWithdrawn),
		AssetWithdrawn: util.IntStr(memberPool.AssetWithdrawn),
		KarmaPending:    util.IntStr(memberPool.KarmaPending),
		AssetPending:   util.IntStr(memberPool.AssetPending),
		DateFirstAdded: util.IntStr(memberPool.DateFirstAdded),
		DateLastAdded:  util.IntStr(memberPool.DateLastAdded),
	}
}

func (memberPool MemberPool) toSavers() oapigen.SaverPool {
	return oapigen.SaverPool{
		Pool:           util.ConvertSynthPoolToNative(memberPool.Pool),
		AssetAddress:   memberPool.AssetAddress,
		AssetAdded:     util.IntStr(memberPool.AssetAdded),
		AssetDeposit:   util.IntStr(memberPool.AssetDeposit),
		SaverUnits:     util.IntStr(memberPool.LiquidityUnits),
		AssetWithdrawn: util.IntStr(memberPool.AssetWithdrawn),
		DateFirstAdded: util.IntStr(memberPool.DateFirstAdded),
		DateLastAdded:  util.IntStr(memberPool.DateLastAdded),
	}
}

// Pools data associated with a single member
type MemberPools []MemberPool

func (memberPools MemberPools) ToOapigen() []oapigen.MemberPool {
	ret := make([]oapigen.MemberPool, len(memberPools))
	for i, memberPool := range memberPools {
		ret[i] = memberPool.toOapigen()
	}

	return ret
}

func (memberPools MemberPools) ToSavers(poolRedeemValueMap map[string]int64) []oapigen.SaverPool {
	ret := make([]oapigen.SaverPool, len(memberPools))
	for i, memberPool := range memberPools {
		ret[i] = memberPool.toSavers()
		ret[i].AssetRedeem = util.IntStr(poolRedeemValueMap[memberPool.Pool])
	}

	return ret
}

type MemberPoolType int

const (
	RegularAndSaverPools MemberPoolType = iota // regular and synth pools too
	RegularPools                               // regular (non-synth) pools e.g. 'BTC.BTC'
	SaverPools                                 // LPs of synth pools e.g. 'BTC/BTC'
)

func PoolBasedOfType(poolName string, poolType MemberPoolType) bool {
	if poolType == RegularAndSaverPools {
		return true
	}
	poolCoinType := record.GetCoinType([]byte(poolName))
	if poolCoinType == record.AssetSynth && poolType == SaverPools {
		return true
	}
	if poolCoinType == record.AssetNative && poolType == RegularPools {
		return true
	}
	return false
}

func GetMemberPools(ctx context.Context, address []string, poolType MemberPoolType) (MemberPools, error) {
	q := `
		SELECT
			pool,
			COALESCE(karma_addr, ''),
			COALESCE(asset_addr, ''),
			lp_units_total,
			asset_e8_deposit,
			karma_e8_deposit,
			added_karma_e8_total,
			added_asset_e8_total,
			withdrawn_karma_e8_total,
			withdrawn_asset_e8_total,
			pending_karma_e8_total,
			pending_asset_e8_total,
			COALESCE(first_added_timestamp / 1000000000, 0),
			COALESCE(last_added_timestamp / 1000000000, 0)
		FROM midgard_agg.members
		WHERE member_id = ANY($1) OR asset_addr = ANY($1)
		ORDER BY pool
	`

	rows, err := db.Query(ctx, q, pq.Array(address))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var results MemberPools
	for rows.Next() {
		var entry MemberPool
		err := rows.Scan(
			&entry.Pool,
			&entry.KarmaAddress,
			&entry.AssetAddress,
			&entry.LiquidityUnits,
			&entry.AssetDeposit,
			&entry.KarmaDeposit,
			&entry.KarmaAdded,
			&entry.AssetAdded,
			&entry.KarmaWithdrawn,
			&entry.AssetWithdrawn,
			&entry.KarmaPending,
			&entry.AssetPending,
			&entry.DateFirstAdded,
			&entry.DateLastAdded,
		)
		if err != nil {
			return nil, err
		}
		if PoolBasedOfType(entry.Pool, poolType) {
			results = append(results, entry)
		}
	}
	return results, nil
}
