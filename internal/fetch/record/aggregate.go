package record

// RunningTotals captures statistics in memory.
type runningTotals struct {
	// running totals
	// TODO(muninn): get rid of the pointers
	assetE8DepthPerPool map[string]*int64
	karmaE8DepthPerPool  map[string]*int64
	synthE8DepthPerPool map[string]*int64
}

func newRunningTotals() *runningTotals {
	return &runningTotals{
		assetE8DepthPerPool: make(map[string]*int64),
		karmaE8DepthPerPool:  make(map[string]*int64),
		synthE8DepthPerPool: make(map[string]*int64),
	}
}

func (t *runningTotals) CurrentDepths(pool []byte) (assetE8, karmaE8, synthE8 int64) {
	if p, ok := t.assetE8DepthPerPool[string(pool)]; ok {
		assetE8 = *p
	}
	if p, ok := t.karmaE8DepthPerPool[string(pool)]; ok {
		karmaE8 = *p
	}
	if p, ok := t.synthE8DepthPerPool[string(pool)]; ok {
		synthE8 = *p
	}
	return
}

// AddPoolAssetE8Depth adjusts the quantity. Use a negative value to deduct.
func (t *runningTotals) AddPoolAssetE8Depth(pool []byte, assetE8 int64) {
	if p, ok := t.assetE8DepthPerPool[string(pool)]; ok {
		*p += assetE8
	} else {
		t.assetE8DepthPerPool[string(pool)] = &assetE8
	}
}

// AddPoolKarmaE8Depth adjusts the quantity. Use a negative value to deduct.
func (t *runningTotals) AddPoolKarmaE8Depth(pool []byte, karmaE8 int64) {
	if p, ok := t.karmaE8DepthPerPool[string(pool)]; ok {
		*p += karmaE8
	} else {
		t.karmaE8DepthPerPool[string(pool)] = &karmaE8
	}
}

// AddPoolSynthE8Depth adjusts the quantity. Use a negative value to deduct.
func (t *runningTotals) AddPoolSynthE8Depth(pool []byte, synthE8 int64) {
	if p, ok := t.synthE8DepthPerPool[string(pool)]; ok {
		*p += synthE8
	} else {
		t.synthE8DepthPerPool[string(pool)] = &synthE8
	}
}

func (t *runningTotals) SetAssetDepth(pool string, assetE8 int64) {
	v := assetE8
	t.assetE8DepthPerPool[pool] = &v
}

func (t *runningTotals) SetKarmaDepth(pool string, karmaE8 int64) {
	v := karmaE8
	t.karmaE8DepthPerPool[pool] = &v
}

func (t *runningTotals) SetSynthDepth(pool string, synthE8 int64) {
	v := synthE8
	t.synthE8DepthPerPool[pool] = &v
}

// AssetE8DepthPerPool returns a snapshot copy.
func (t *runningTotals) AssetE8DepthPerPool() map[string]int64 {
	m := make(map[string]int64, len(t.assetE8DepthPerPool))
	for asset, p := range t.assetE8DepthPerPool {
		m[asset] = *p
	}
	return m
}

// KarmaE8DepthPerPool returns a snapshot copy.
func (t *runningTotals) KarmaE8DepthPerPool() map[string]int64 {
	m := make(map[string]int64, len(t.karmaE8DepthPerPool))
	for asset, p := range t.karmaE8DepthPerPool {
		m[asset] = *p
	}
	return m
}

// SynthE8DepthPerPool returns a snapshot copy.
func (t *runningTotals) SynthE8DepthPerPool() map[string]int64 {
	m := make(map[string]int64, len(t.synthE8DepthPerPool))
	for asset, p := range t.synthE8DepthPerPool {
		m[asset] = *p
	}
	return m
}
