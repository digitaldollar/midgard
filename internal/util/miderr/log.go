package miderr

import "gitlab.com/digitaldollar/midgard/internal/util/midlog"

func LogEventParseErrorF(format string, v ...interface{}) {
	midlog.WarnF(format, v...)
}
