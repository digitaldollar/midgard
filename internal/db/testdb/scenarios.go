package testdb

import (
	"testing"
)

// A test scenario with 10 swaps
// 4x karma -> asset: volume 40, fee 4, slip 5
// 3x asset -> karma: volume 3000 + 300, fee 300, slip 6
// 2x karma -> synth: volume 200000, fee 20000, slip 7
// 1x synth -> karma: volume 10000000 + 1000000, fee 1000000, slip 8
func ScenarioTenSwaps(t *testing.T, blocks *blockCreator) {
	blocks.NewBlock(t, "2010-01-01 00:00:00",
		AddLiquidity{
			Pool:        "BTC.BTC",
			KarmaAddress: "ddaddr1",
			AssetAmount: 1000000,
			KarmaAmount:  10000000,
		},
		PoolActivate("BTC.BTC"),
	)

	// 4x karma -> asset
	blocks.NewBlock(t, "2020-01-01 00:01:00",
		Swap{
			Pool:               "BTC.BTC",
			Coin:               "10 DD.KARMA",
			EmitAsset:          "1 BTC.BTC",
			LiquidityFeeInKarma: 1,
			Slip:               5,
		},
		Swap{
			Pool:               "BTC.BTC",
			Coin:               "10 DD.KARMA",
			EmitAsset:          "1 BTC.BTC",
			LiquidityFeeInKarma: 1,
			Slip:               5,
		},
		Swap{
			Pool:               "BTC.BTC",
			Coin:               "10 DD.KARMA",
			EmitAsset:          "1 BTC.BTC",
			LiquidityFeeInKarma: 1,
			Slip:               5,
		},
		Swap{
			Pool:               "BTC.BTC",
			Coin:               "10 DD.KARMA",
			EmitAsset:          "1 BTC.BTC",
			LiquidityFeeInKarma: 1,
			Slip:               5,
		})

	// 3x asset -> karma
	blocks.NewBlock(t, "2020-01-01 00:02:00",
		Swap{
			Pool:               "BTC.BTC",
			Coin:               "100 BTC.BTC",
			EmitAsset:          "1000 DD.KARMA",
			LiquidityFeeInKarma: 100,
			LiquidityFee:       100,
			Slip:               6,
		},
		Swap{
			Pool:               "BTC.BTC",
			Coin:               "100 BTC.BTC",
			EmitAsset:          "1000 DD.KARMA",
			LiquidityFeeInKarma: 100,
			LiquidityFee:       100,
			Slip:               6,
		},
		Swap{
			Pool:               "BTC.BTC",
			Coin:               "100 BTC.BTC",
			EmitAsset:          "1000 DD.KARMA",
			LiquidityFeeInKarma: 100,
			LiquidityFee:       100,
			Slip:               6,
		})

	// 2x karma -> synth
	blocks.NewBlock(t, "2020-01-01 00:03:00",
		Swap{
			Pool:               "BTC.BTC",
			Coin:               "100000 DD.KARMA",
			EmitAsset:          "10000 BTC/BTC",
			LiquidityFeeInKarma: 10000,
			Slip:               7,
		},
		Swap{
			Pool:               "BTC.BTC",
			Coin:               "100000 DD.KARMA",
			EmitAsset:          "10000 BTC/BTC",
			LiquidityFeeInKarma: 10000,
			Slip:               7,
		})

	// 1x synth -> karma
	blocks.NewBlock(t, "2020-01-01 00:04:00",
		Swap{
			Pool:               "BTC.BTC",
			Coin:               "1000000 BTC/BTC",
			EmitAsset:          "10000000 DD.KARMA",
			LiquidityFeeInKarma: 1000000,
			Slip:               8,
		},
	)

}
