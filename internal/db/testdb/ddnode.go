package testdb

import (
	"net/http"

	"github.com/jarcoal/httpmock"
	"github.com/rs/zerolog/log"
	"gitlab.com/digitaldollar/midgard/internal/fetch/notinchain"
)

const ddNodeUrl = "http://ddnode.com"

// Starts Ddnode HTTP mock with some simiple / empty results.
func StartMockDdnode() (deactivateCallback func()) {
	notinchain.BaseURL = ddNodeUrl
	httpmock.Activate()

	setInitialDdnodeConstants()

	RegisterDdnodeNodes([]notinchain.NodeAccount{})
	RegisterDdnodeReserve(0)

	return func() {
		httpmock.DeactivateAndReset()
	}
}

func RegisterDdnodeNodes(nodeAccounts []notinchain.NodeAccount) {
	httpmock.RegisterResponder("GET", ddNodeUrl+"/nodes",
		func(req *http.Request) (*http.Response, error) {
			resp, err := httpmock.NewJsonResponse(200, nodeAccounts)
			if err != nil {
				return httpmock.NewStringResponse(500, ""), nil
			}
			return resp, nil
		},
	)
}

func RegisterDdnodeReserve(totalReserve int64) {
	httpmock.RegisterResponder("GET", ddNodeUrl+"/network",
		func(req *http.Request) (*http.Response, error) {
			vaultData := notinchain.Network{TotalReserve: totalReserve}
			resp, err := httpmock.NewJsonResponse(200, vaultData)
			if err != nil {
				return httpmock.NewStringResponse(500, ""), nil
			}
			return resp, nil
		},
	)
}

// Sets some non 0 values for the constants. To have some meaningful values in tests
// overwrite them with mimir.
// Assumes httpmock.Activate has been called
func setInitialDdnodeConstants() {

	constants := notinchain.Constants{Int64Values: map[string]int64{
		"EmissionCurve":      1234,
		"BlocksPerYear":      1234,
		"ChurnInterval":      1234,
		"ChurnRetryInterval": 1234,
		"PoolCycle":          1234,
		"IncentiveCurve":     1234,
		"MinimumBondInKarma":  1234,
	}}
	httpmock.RegisterResponder("GET", ddNodeUrl+"/constants",
		func(req *http.Request) (*http.Response, error) {
			resp, err := httpmock.NewJsonResponse(200, constants)
			if err != nil {
				return httpmock.NewStringResponse(500, ""), nil
			}
			return resp, nil
		},
	)
	err := notinchain.LoadConstants()
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to read constants")
	}
}
