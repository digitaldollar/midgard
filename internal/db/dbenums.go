package db

type SwapDirection int8

// Do not change these constantss. SQL Queries may assume this value dirrectly.
const (
	KarmaToAsset   SwapDirection = 0
	AssetToKarma   SwapDirection = 1
	KarmaToSynth   SwapDirection = 2
	SynthToKarma   SwapDirection = 3
	KarmaToDerived SwapDirection = 4
	DerivedToKarma SwapDirection = 5
)
