INSERT INTO midgard_agg.watermarks (materialized_table, watermark)
    VALUES ('karma_price', 0);

CREATE TABLE midgard_agg.karma_price (
    karma_price_usd DOUBLE PRECISION NOT NULL,
    block_timestamp bigint NOT NULL,
    PRIMARY KEY(block_timestamp)
);

-- TODO(hooriRN): fill with actual price instead of a constant
CREATE PROCEDURE midgard_agg.update_karma_price_interval(t1 bigint, t2 bigint)
LANGUAGE SQL AS $BODY$
    INSERT INTO midgard_agg.karma_price AS cb (
        SELECT 
            1 AS karma_price_usd,
            timestamp as block_timestamp
        FROM block_log
        WHERE t1 <= timestamp AND timestamp < t2
    )
    ON CONFLICT (block_timestamp) DO UPDATE SET karma_price_usd = cb.karma_price_usd;
$BODY$;

CREATE PROCEDURE midgard_agg.update_karma_price(w_new bigint)
LANGUAGE plpgsql AS $BODY$
DECLARE
    w_old bigint;
BEGIN
    SELECT watermark FROM midgard_agg.watermarks WHERE materialized_table = 'karma_price'
        FOR UPDATE INTO w_old;
    IF w_new <= w_old THEN
        RAISE WARNING 'Updating karma prices into past: % -> %', w_old, w_new;
        RETURN;
    END IF;
    CALL midgard_agg.update_karma_price_interval(w_old, w_new);
    UPDATE midgard_agg.watermarks SET watermark = w_new WHERE materialized_table = 'karma_price';
END
$BODY$;