#!/bin/bash

set -x

pwd
cd ddnode

pwd

docker rm -f $(docker ps -a -q)

# # If the reset-mocknet-standalone can not delete the following files
# # add this rm to remove all files from ~/.ddnode/standalone
# sudo rm -r $HOME/.ddnode/standalone/.b* $HOME/.ddnode/standalone/.t*

make -C build/docker reset-mocknet-standalone

sleep 1

docker stop midgard
docker stop timescale-db

sleep

cd ../midgard
pwd

docker-compose up -d pgtest

sleep 1

go run cmd/midgard/main.go config/config.json
