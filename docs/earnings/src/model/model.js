'use strict';

class LPLiquidity {
    // Return on investment as measured in USD, KARMA or the pool asset.

    constructor() {
    }
    update(memberDetails, poolDetails, actions, assetPriceInKarmaByTime, assetPriceInUsdByTime) {
        this.pool = memberDetails.pool;
        this.memberDetails = memberDetails;
        this.poolDetails = poolDetails;
        this.updateAddWithdrawnValueInKarma(actions, assetPriceInKarmaByTime, assetPriceInUsdByTime);

        // TODO(leifthelucky): Check how to divide this correctly without losing precision.
        this.redeemableKarma = memberDetails.liquidityUnits / poolDetails.liquidityUnits * poolDetails.karmaDepth;
        this.redeemableAsset = memberDetails.liquidityUnits / poolDetails.liquidityUnits * poolDetails.assetDepth;

        this.realizedReturnValueInKarma = this.withdrawnValueInKarma - this.addedValueInKarma;
        this.realizedReturnValueInAsset = this.withdrawnValueInAsset - this.addedValueInAsset;
        this.realizedReturnValueInUsd = this.withdrawnValueInUsd - this.addedValueInUsd;

        this.redeemableValueInKarma = this.redeemableKarma + this.redeemableAsset * poolDetails.assetPrice;
        this.redeemableValueInAsset = this.redeemableKarma / poolDetails.assetPrice + this.redeemableAsset;
        this.redeemableValueInUsd = this.redeemableKarma / poolDetails.assetPrice * poolDetails.assetPriceUSD + this.redeemableAsset * poolDetails.assetPriceUSD;

        this.totalReturnValueInKarma = this.realizedReturnValueInKarma + this.redeemableValueInKarma;
        this.totalReturnValueInAsset = this.realizedReturnValueInAsset + this.redeemableValueInAsset;
        this.totalReturnValueInUsd = this.realizedReturnValueInUsd + this.redeemableValueInUsd;
    }
    updateAddWithdrawnValueInKarma(actions, assetPriceInKarmaByTime, assetPriceInUsdByTime) {
        this.addedKarma = 0;
        this.addedAsset = 0;
        this.addedValueInKarma = 0;
        this.addedValueInAsset = 0;
        this.addedValueInUsd = 0;
        this.withdrawnKarma = 0;
        this.withdrawnAsset = 0;
        this.withdrawnValueInKarma = 0;
        this.withdrawnValueInAsset = 0;
        this.withdrawnValueInUsd = 0;
        for (const action of actions) {
            if (action.status != "success") {
                continue;
            }
            if (action.pools.length != 1 || action.pools[0] != this.pool) {
                continue;
            }
            const valueInKarma = function (coin, assetFilter, assetPriceKarma) {
                switch (coin.asset) {
                    case "DD.KARMA":
                        return Number(coin.amount);
                    case assetFilter:
                        return Number(coin.amount) * assetPriceKarma;
                    default:
                        return;
                }
            }
            const valueInAsset = function (coin, assetFilter, assetPriceKarma) {
                switch (coin.asset) {
                    case "DD.KARMA":
                        return Number(coin.amount) / assetPriceKarma;
                    case assetFilter:
                        return Number(coin.amount);
                    default:
                        return;
                }
            }
            const valueInUsd = function (coin, assetFilter, assetPriceKarma, assetPriceUsd) {
                switch (coin.asset) {
                    case "DD.KARMA":
                        return Number(coin.amount) / assetPriceKarma * assetPriceUsd;
                    case assetFilter:
                        return Number(coin.amount) * assetPriceUsd;
                    default:
                        return;
                }
            }
            const s = Math.floor(action.date / 1e9);
            const assetPriceKarma = assetPriceInKarmaByTime[s];
            const assetPriceUsd = assetPriceInUsdByTime[s];
            let inKarma = 0;
            let inAsset = 0;
            let inValueInKarma = 0;
            let inValueInAsset = 0;
            let inValueInUsd = 0;
            let outKarma = 0;
            let outAsset = 0;
            let outValueInKarma = 0;
            let outValueInAsset = 0;
            let outValueInUsd = 0;
            for (const inputs of action.in) {
                for (const coin of inputs.coins) {
                    inKarma += (coin.asset == "DD.KARMA") ? Number(coin.amount) : 0;
                    inAsset += (coin.asset == this.pool) ? Number(coin.amount) : 0;
                    inValueInKarma += valueInKarma(coin, this.pool, assetPriceKarma);
                    inValueInAsset += valueInAsset(coin, this.pool, assetPriceKarma);
                    inValueInUsd += valueInUsd(coin, this.pool, assetPriceKarma, assetPriceUsd);
                }
            }
            for (const inputs of action.out) {
                for (const coin of inputs.coins) {
                    outKarma += (coin.asset == "DD.KARMA") ? Number(coin.amount) : 0;
                    outAsset += (coin.asset == this.pool) ? Number(coin.amount) : 0;
                    outValueInKarma += valueInKarma(coin, this.pool, assetPriceKarma);
                    outValueInAsset += valueInAsset(coin, this.pool, assetPriceKarma);
                    outValueInUsd += valueInUsd(coin, this.pool, assetPriceKarma, assetPriceUsd);
                }
            }
            switch (action.type) {
                case "withdraw":
                    // In karma/asset/value contain the amount in the withdraw
                    // transaction, that is the one with the withdraw memo.
                    this.withdrawnKarma += outKarma - inKarma;
                    this.withdrawnAsset += outAsset - inAsset;
                    this.withdrawnValueInKarma += outValueInKarma - inValueInKarma;
                    this.withdrawnValueInAsset += outValueInAsset - inValueInAsset;
                    this.withdrawnValueInUsd += outValueInUsd - inValueInUsd;
                    break;
                case "addLiquidity":
                    // Out karma/asset/value should be 0 for addLiquidity events,
                    // but we include them here just in case.
                    this.addedKarma += inKarma - outKarma;
                    this.addedAsset += inAsset - outAsset;
                    this.addedValueInKarma += inValueInKarma - outValueInKarma;
                    this.addedValueInAsset += inValueInAsset - outValueInAsset;
                    this.addedValueInUsd += inValueInUsd - outValueInUsd;
                    break;
            }
        }
    }
};

g.m.LPLiquidity = new LPLiquidity();

