'use strict';

g.v.updatePoolReturns = function (elem, poolReturns) {
    let p = poolReturns;
    // In Ddnode/Midgard, to store certain values as integers without losing precision,
    // the values are scaled up by 1e8 before being truncated. Undo this scaling for
    // presentation purposes.
    for (let a of ['Karma', 'Asset']) {
        for (let metric of ['added', 'withdrawn', 'redeemable']) {
            p[metric + a] = p[metric + a] * 1e-8;
        }
    }
    for (let a of ['Usd', 'Karma', 'Asset']) {
        for (let metric of ['addedValueIn', 'withdrawnValueIn', 'redeemableValueIn',
                            'realizedReturnValueIn', 'totalReturnValueIn']) {
            p[metric + a] = p[metric + a] * 1e-8;
        }
    }
    elem.innerHTML = `
        <h3>Position Summary</h3>
        <table><tr>
                <th></th><th>KARMA</th><th>Asset</th>
            </tr><tr>
                <td>Added</td><td>${p.addedKarma}</td><td>${p.addedAsset}</td>
            </tr><tr>
                <td>Withdrawn</td><td>${p.withdrawnKarma}</td><td>${p.withdrawnAsset}</td>
            </tr><tr>
                <td>Redeemable</td><td>${p.redeemableKarma}</td><td>${p.redeemableAsset}</td>
            </tr><tr>
                <td>Gain (Reedeemable + Withdrawn - Added)</td>
                <td>${(p.redeemableKarma + p.withdrawnKarma - p.addedKarma)}</td>
                <td>${(p.redeemableAsset + p.withdrawnAsset - p.addedAsset)}</td>
            </tr><tr>
                <td>Gain % ((Redeemable + Withdrawn) / Added - 1)</td>
                <td>${((p.redeemableKarma + p.withdrawnKarma) / p.addedKarma - 1) * 100}%</td>
                <td>${((p.redeemableAsset + p.withdrawnAsset) / p.addedAsset - 1) * 100}%</td>
            </tr>
        </table>
        <h3>Valuations</h3>
        <table><tr>
                <th></th><th>Method 1: USD</th><th>Method 2: KARMA</th><th>Method 3: Asset</th>
            </tr><tr>
                <td>Added value</td><td>${p.addedValueInUsd}</td><td>${p.addedValueInKarma}</td><td>${p.addedValueInAsset}</td>
            </tr><tr>
                <td>Withdrawn value</td><td>${p.withdrawnValueInUsd}</td><td>${p.withdrawnValueInKarma}</td><td>${p.withdrawnValueInAsset}</td>
            </tr><tr>
                <td>Redeemable value</td><td>${p.redeemableValueInUsd}</td><td>${p.redeemableValueInKarma}</td><td>${p.redeemableValueInAsset}</td>
            </tr><tr>
                <td>Realized return value</td><td>${p.realizedReturnValueInUsd}</td><td>${p.realizedReturnValueInKarma}</td><td>${p.realizedReturnValueInAsset}</td>
            </tr><tr>
                <td>Total return value</td><td>${p.totalReturnValueInUsd}</td><td>${p.totalReturnValueInKarma}</td><td>${p.totalReturnValueInAsset}</td>
            </tr>
        </table>`;
}
